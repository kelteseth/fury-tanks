using Godot;
using System;

public class Root : Spatial
{

    public override void _Ready()
    {
        var server = new NetworkedMultiplayerENet();
        server.CreateServer(16395,3);
        GetTree().SetNetworkPeer(server);
        GetTree().Connect("network_peer_connected", this, nameof(OnClientConnected));
    }


    
    public void OnClientConnected(int i){
        GD.Print("OnClientConnected");   
    }
//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
