<div>
<img width="100%" height="300" src="https://screen-play.app/images/trailerfury_logo_gitlab_fullwidth.svg">
</div>

<div align="center">

</div>
<br>

# Getting started

### Requirements
1. Git
2. Godot 3.1.1 Mono version (C# support)
3. Git Symlink support. Aka Windows 10 1703 developer mode activated before cloning.

``` bash
# HTTPS
git clone https://gitlab.com/kelteseth/fury-tanks.git
```

### Credits
 - [MrMinimal](https://www.reddit.com/user/mrminimal)
 - [Kelteseth](https://www.reddit.com/user/kelteseth)

### Assets
 - [flaticon](https://www.flaticon.com/)
 - [freesound](https://freesound.org/)
 - [youtube.com/audiolibrary](https://www.youtube.com/audiolibrary/soundeffects)
