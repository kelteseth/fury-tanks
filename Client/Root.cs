using Godot;
using System;

public class Root : Spatial
{

    private string playername = "elias";

    public override void _Ready()
    {
        GetTree().Connect("connected_to_server", this, nameof(ConnectedToServer));
         GetTree().Connect("connection_failed", this, nameof(ConnectionFailed));
    }

    public void ConnectToServer(String address){
        var client = new  NetworkedMultiplayerENet();
        client.CreateClient(address, 16395);
        GetTree().SetNetworkPeer(client);
    }

    public void ConnectionFailed(){
         GD.Print("ConnectionFailed");
    }

    public void ConnectedToServer(){
        GD.Print("Connected");
        var map  = (PackedScene)ResourceLoader.Load("res://Common/Maps/Battlefield/Battlefield.tscn");
        GetNode("/root/Root/Maps").AddChild(map.Instance());      
        var tank  = (PackedScene)ResourceLoader.Load("res://Common/Assets/Models/Tanks/Tank.tscn");
        var tankInstance = tank.Instance();
        tankInstance.SetName("Tank-" + playername);
        GetNode("/root/Root/Players").AddChild(tankInstance);      
        var camera = new Camera();
        tankInstance.AddChild(camera);
        camera.SetTranslation(new Vector3(0, 6,6));
        camera.SetRotationDegrees(new Vector3(-25,0,0));
        var lobby = GetNode("Lobby");
        lobby.QueueFree();
        
    }
//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
