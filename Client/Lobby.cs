using Godot;
using System;

public class Lobby : Spatial
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        
    }

    private void _on_Button_pressed()
    {
        
        var textBox = (TextEdit)GetNode("Control/CenterContainer/VBoxContainer/TextEditIpAdress") ;
        GD.Print(textBox.Text.ToString());
        
        var rootRef = (Root)GetNode("/root/Root");
        rootRef.ConnectToServer(textBox.Text.ToString());
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}


